package com.automation.app.ui.device;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.automation.app.R;
import com.automation.app.TCPClient;
import com.automation.app.ui.DeviceStatus;

import java.util.Arrays;

public class DeviceFragment extends Fragment {

    private DeviceViewModel deviceViewModel;
    private ImageButton device1;
    private ImageButton device2;
    private ImageButton device3;
    private ImageButton device4;

    private ImageButton[] imageButtons =  new ImageButton[4];
    private boolean[] deviceStatus = DeviceStatus.getDeviceStatus();
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        deviceViewModel =
                new ViewModelProvider(this).get(DeviceViewModel.class);
        View root = inflater.inflate(R.layout.fragment_device, container, false);

        device1 = root.findViewById(R.id.device1);
        device2 = root.findViewById(R.id.device2);
        device3 = root.findViewById(R.id.device3);
        device4 = root.findViewById(R.id.device4);
        imageButtons[0] = device1;
        imageButtons[1] = device2;
        imageButtons[2] = device3;
        imageButtons[3] = device4;

        for (int i = 0;i< deviceStatus.length;i++){
            if(deviceStatus[i]){
                imageButtons[i].setBackgroundResource(R.drawable.outline_emoji_objects_black_48);
            }else {
                imageButtons[i].setBackgroundResource(R.drawable.baseline_emoji_objects_black_48);

            }
        }
        device1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeDeviceState(1);
            }
        });
        device2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeDeviceState(2);
            }
        });
        device3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeDeviceState(3);
            }
        });
        device4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeDeviceState(4);
            }
        });
        return root;
    }

    private void changeDeviceState(int deviceNo){
        if(!deviceStatus[deviceNo-1]){
            imageButtons[deviceNo-1].setBackgroundResource(R.drawable.outline_emoji_objects_black_48);
            byte[] data = new byte[4];
            data[0] = '*';
            data[1] = (byte) deviceNo;
            data[2] = 1;
            data[3] = '#';
            deviceStatus[deviceNo-1] = true;
            DeviceStatus.setDeviceStatus(deviceStatus);
            TCPClient.getInstance().sendMessage((data));
        }else {
            imageButtons[deviceNo-1].setBackgroundResource(R.drawable.baseline_emoji_objects_black_48);
            byte[] data = new byte[4];
            data[0] = '*';
            data[1] = (byte) deviceNo;
            data[2] = 0;
            data[3] = '#';
            deviceStatus[deviceNo-1] = false;
            DeviceStatus.setDeviceStatus(deviceStatus);
            TCPClient.getInstance().sendMessage((data));
        }
    }
}