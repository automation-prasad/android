package com.automation.app.ui;

public class DeviceStatus {
    private static boolean[] deviceStatus = {true,true,true,true};

    private static String mcuStatus = "DISCONNECTED";

    public static boolean[] getDeviceStatus() {
        return deviceStatus;
    }

    public static void setDeviceStatus(boolean[] deviceStatus) {
        DeviceStatus.deviceStatus = deviceStatus;
    }

    public static String getMcuStatus() {
        return mcuStatus;
    }

    public static void setMcuStatus(String mcuStatus) {
        DeviceStatus.mcuStatus = mcuStatus;
    }
}

