package com.automation.app.ui.settings;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.automation.app.R;
import com.automation.app.TCPClient;
import com.automation.app.ui.DeviceStatus;

public class settingsFragment extends Fragment {

    private settingsViewModel settingsViewModel;
    TCPClient mTcpClient = TCPClient.getInstance();
    private Button connectButton;
    private TextView connectionStatus;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        settingsViewModel =
                new ViewModelProvider(this).get(settingsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_settings, container, false);

        connectButton = root.findViewById(R.id.connect);
        connectionStatus = root.findViewById(R.id.connect_status_text);
        connectionStatus.setText(DeviceStatus.getMcuStatus());
        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ConnectTask().execute("");
            }
        });

        connectionStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return root;
    }


    public class ConnectTask extends AsyncTask<String, String, TCPClient> {

        @Override
        protected TCPClient doInBackground(String... message) {

            //we create a TCPClient object
            mTcpClient.init(new TCPClient.OnMessageReceived() {
                @Override
                //here the messageReceived method is implemented
                public void messageReceived(byte[] message) {
                    //this method calls the onProgressUpdate
                    publishProgress("DATA", String.valueOf(message));
                }

                @Override
                public void onConnectionChanged(String message) {
                    publishProgress("CONNECTION",message);
                }
            });
            mTcpClient.run();
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            //response received from server
            Log.d("test", "response " + values[0] + " " + values[1]);
            if(values[0].equals("CONNECTION")){
                connectionStatus.setText(values[1]);
                if(values[1].equals("Connected")){
                    // Send message after connection to request latest device state
                        byte[] data = new byte[4];
                        data[0] = '#';
                        data[1] = 4;
                        data[2] = 4;
                        data[3] = '*';
                        TCPClient.getInstance().sendMessage((data));
                }
            }
            else {
                Log.d("Data",    values[1]);
            }
            //process server response here....

        }
    }
}