package com.automation.app;

import android.util.Log;

import com.automation.app.ui.DeviceStatus;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class TCPClient {

    public static final String TAG = TCPClient.class.getSimpleName();
    public static final String SERVER_IP = "192.168.43.199"; //server IP address
    public static final int SERVER_PORT = 80;
    private static TCPClient mTCPClient;
    // message to send to the server
    private byte[] mServerMessage;
    // sends message received notifications
    private OnMessageReceived mMessageListener = null;
    // while this is true, the server will continue running
    private boolean mRun = false;
    // used to send messages
    private DataInputStream mBufferIn;
    // used to read messages from the server
    private OutputStream mBufferOut;

    public void init(OnMessageReceived mMessageListener) {
        this.mMessageListener = mMessageListener;
    }

    public static TCPClient getInstance(){
        synchronized (TCPClient.class){
            if(mTCPClient == null){
                mTCPClient = new TCPClient();
            }
        }
        return mTCPClient;
    }
    /**
     * Sends the message entered by client to the server
     *
     * @param data text entered by client
     */
    public void sendMessage(final byte[] data) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (mBufferOut != null) {
                    Log.d(TAG, "Sending: " + data);
                    try {
                        mBufferOut.write(data);
                        mBufferOut.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    /**
     * Close the connection and release the members
     */
    public void stopClient() {

        mRun = false;

        if (mBufferOut != null) {
            try {
                mBufferOut.flush();
                mBufferOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        mMessageListener = null;
        mBufferIn = null;
        mBufferOut = null;
        mServerMessage = null;
    }

    public void run(){

        mRun = true;
        Socket socket = null;
        InetAddress serverAddr;
        try{
            serverAddr = InetAddress.getByName(SERVER_IP);
            Log.d("TCP Client", "Connecting...");
            //create a socket to make the connection with the server
            socket = new Socket(serverAddr, SERVER_PORT);
        }
        catch (IOException e){
            if(e != null)
                e.printStackTrace();
            if (mMessageListener != null) {
                //call the method messageReceived from MyActivity class
                mMessageListener.onConnectionChanged("Failed to create");
            }
            return;
        }

        try {

            if (mMessageListener != null) {
                //call the method messageReceived from MyActivity class
                mMessageListener.onConnectionChanged("Connected");
                DeviceStatus.setMcuStatus("Connected");
            }

            mBufferIn = new DataInputStream(socket.getInputStream());
            mBufferOut = socket.getOutputStream();

            //in this while the client listens for the messages sent by the server
            while (mRun) {
                try {
                    if (mBufferIn != null) {
                        byte[] mServerMessage = new byte[4];
                        mBufferIn.readFully(mServerMessage);
                        if (mMessageListener != null) {
                            //call the method messageReceived from MyActivity class
                            mMessageListener.messageReceived(mServerMessage);
                        }
                    }
                    else {
                        DeviceStatus.setMcuStatus("Disconnected");
                        stopClient();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    if (mMessageListener != null) {
                        //call the method messageReceived from MyActivity class
                        mMessageListener.onConnectionChanged("Disconnected");
                        DeviceStatus.setMcuStatus("Disconnected");
                    }
                }
            }

            Log.d("RESPONSE FROM SERVER", "S: Received Message: '" + mServerMessage + "'");

        } catch (Exception e) {
            Log.e("TCP", "S: Error", e);
            if (mMessageListener != null) {
                //call the method messageReceived from MyActivity class
                mMessageListener.onConnectionChanged("Disconnected");
                DeviceStatus.setMcuStatus("Disconnected");
            }
        } finally {
            //the socket must be closed. It is not possible to reconnect to this socket
            // after it is closed, which means a new socket instance has to be created.
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //Declare the interface. The method messageReceived(String message) will must be implemented in the Activity
    //class at on AsyncTask doInBackground
    public interface OnMessageReceived {
        void messageReceived(byte[] message);
        void onConnectionChanged(String message);
    }

}
